-- TODO : command to restart game
-- TODO : command to get sum up of players
-- TODO : time limit
-- TODO : available in /s

SLASH_DBJ1 = "/dbj"

game_leader = nil
--env = "PARTY"
env = "GUILD"
--env = "SAY"
env_listener = "CHAT_MSG_GUILD"
--env_listener = "CHAT_MSG_SAY"
--env_listener = "CHAT_MSG_PARTY_LEADER"
new_party = 0;
--initGame();

SlashCmdList["DBJ"] = function(msg)
	if msg == 'new' then
--		SendChatMessage("Une nouvelle partie commence, tapez 'dbj join' pour participer", env)
--		SendChatMessage("Écrivez 'dbj rand' pour miser, 'dbj stop' pour arretez votre mise, le plus proche de 21 a gagné", env)
		SendChatMessage("Nouvelle partie", env)
		initGame();
		new_party = 1;
	end
end

local ChatFrame = CreateFrame("Frame")
ChatFrame:RegisterEvent(env_listener)
ChatFrame:RegisterEvent("CHAT_MSG_PARTY")

ChatFrame:SetScript("OnEvent", function(self, event, message, sender)
	gameCases(sender, message)
end)

function gameCases(sender, message)
	if message == 'dbj new' then
		if new_party == 1 then
			SendChatMessage("Partie déjà en cours", env)
		else
			SendChatMessage("Nouvelle partie", env)
			initGame();
			new_party = 1;
		end
	elseif message == 'dbj join' then
		if new_party == 1 then
			SendChatMessage(sender .. " rejoint la partie", env)
			players_bids[sender] = 0
		else
			SendChatMessage("Pas de partie en cours, faites '/dbj new' pour commencer une nouvelle partie", env)
		end
	elseif message == 'dbj rand' then
		if new_party == 1 then
			if players_stop[sender] == 1 then
				SendChatMessage(sender .. " ne peut plus jouer (dernière mise à " .. players_bids[sender] .. ")", env);
			else
				rand = math.random(10);
				players_bids[sender] = players_bids[sender] + rand;
				if players_bids[sender] < 21 then
					SendChatMessage(sender .. " est à " .. players_bids[sender], env);
				elseif players_bids[sender] == 21 then
					SendChatMessage(sender .. " est à " .. players_bids[sender] .. " - Death Black Jack", env);
					players_stop[sender] = 1;
				else
					SendChatMessage(sender .. " à perdu (" .. players_bids[sender] .. ")", env)
					players_stop[sender] = 1;
				end
			end
			stopGame();
		else
			SendChatMessage("Pas de partie en cours, le maître du jeu doit commencer une nouvelle partie", env)
		end
	elseif message == 'dbj stop' and new_party == 1 then
		SendChatMessage(sender .. " stop à " .. players_bids[sender], env)
		players_stop[sender] = 1
		stopGame();
	elseif message == 'dbj gamestop' and new_party == 1 then
		SendChatMessage("La partie est annulée", env)
		initGame()
	elseif message == 'dbj test' then
		SendChatMessage(" test events ", env)
	end
end

function stopGame()
	SendChatMessage(" Nombre de joueurs stop " .. tablelength(players_stop) .. "/" .. tablelength(players_bids), env)
	isHe = isLastGamerWon();
	if tablelength(players_bids) <= tablelength(players_stop) and isHe == false then
		winner = getWinner(players_bids);
		if winner == nil then
			SendChatMessage("Tous les joueurs ont arrété, pas de gagnant", env)
		else
			SendChatMessage("Tous les joueurs ont arrété, le gagnant est " .. winner .. " avec " .. players_bids[winner], env)
		end
		initGame()
	end
end

function tablelength(T)
  local count = 0
  for k, v in pairs(T) do
	  if v ~= nil then
	  	count = count + 1
	  end
  end
  return count
end

function isLastGamerWon()
	nbPlaying = tablelength(players_bids) - tablelength(players_stop);
	SendChatMessage("Il reste " .. nbPlaying .. " joueurs actifs", env)
	if tablelength(players_bids) > 1 and tablelength(players_stop) > 1 and tablelength(players_bids) - tablelength(players_stop) == 1 then
		winner = getWinner(players_bids)
		-- if the last personn play if above others, he wins
		if winner == sender then
			SendChatMessage("Le gagnant est " .. winner .. " avec " .. players_bids[winner], env)
			initGame()
			return true;
		end
		return false;
	end
	return false;
end

function getWinner(players_rand)
	rand_winner_name = nil;
	rand_winner_value = 0;
	exaequo = 0;
	for player_name, player_rand in pairs(players_rand) do
		if rand_winner_name == nil and player_rand <= 21 then
			rand_winner_name = player_name;
			rand_winner_value = player_rand;
		elseif player_rand == rand_winner_value and player_rand <= 21 then
			exaequo = 1;
			rand_winner_name = nil;
			rand_winner_value = 0;
		else
			if player_rand > rand_winner_value and player_rand <= 21 then
				rand_winner_value = player_rand
				rand_winner_name = player_name;
				exaequo = 0;
			end
		end
	end

	return rand_winner_name
end

local function hasValue (tab, val)
    for index, value in ipairs(tab) do
        if value == val then
            return true
        end
    end

    return false
end

function initGame()
	players_bids = {}
	players_stop = {}
	new_party = 0
end


--local ChatSayFrame = CreateFrame("Frame")
--ChatSayFrame:RegisterEvent("CHAT_MSG_SAY")
--ChatFrame:SetScript("OnEvent", function(self, event, message, sender)
--	if message:lower():match("hello") then
--        SendChatMessage("hi", "SAY");
--	end
--end)

--local SystemFrame = CreateFrame('Frame')
--SystemFrame:RegisterEvent("SYSMSG")
--SystemFrame:SetScript("OnEvent", function(self, event, message, sender, ...)
--	SendChatMessage("Rand fait", "PARTY")
--end)